<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;

class MapController extends Controller
{
    public function show()
    {
        $markers = DB::select('select * from pins');

        return view('map', [
            'markers' => $markers
        ]);
    }

    public function add(Request $request)
    {
        // Validation du formulaire basique
        $this->validate($request, [
          'name'     =>  'required',
          'message' =>  'required',
          'mapvalue' =>  'required'
         ]);

         // Récupération des coordonnées indiquées
        preg_match_all('!\d+!', $request->mapvalue, $matches);
        
        // vérification serveur plus poussée pour valider les coordonées
        // vérif si il y a bien au moins deux coordonées, puis si elle son bien entre 0 et 100
        if (count($matches)<1)
            return Redirect::back()->withErrors(['msg' => 'Erreur dans vos coordonnées renseignées.']);
        if ( $matches[0][0] > 100 | $matches[0][1] > 100)
            return Redirect::back()->withErrors(['msg' => 'Erreur dans vos coordonnées renseignées.']);
            
        //récupération des markers en BDD
        $markers = DB::select('select * from pins');

        // formatage de la requêtedu formulaire pour l'ajouter aux autres markers
        // !! WARNING !! Utilisation de nombres en dur pour la démo
        // id = 4, order = 4 à modifier plus tard
        $tempmark = new \stdClass();
        $tempmark->name = $request->name;
        $tempmark->posx = $matches[0][0]."%";
        $tempmark->posy = $matches[0][1]."%";
        $tempmark->id = 4;
        $tempmark->order = 4;

        // On ajoute le nouveau markers à la liste des markers
        array_push($markers, $tempmark);

        return view('map', [
            'markers' => $markers
        ]);
    }
}
