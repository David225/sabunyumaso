<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $markers = array();

        $tempmark = new \stdClass();
        $tempmark->name = "Village de Gani";
        $tempmark->posx = "51%";
        $tempmark->posy = "6%";
        $tempmark->id = 1;
        $tempmark->order = 1;
        $tempmark->folder = "gani";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Festival équestre de Barani";
        $tempmark->posx = "56%";
        $tempmark->posy = "30%";
        $tempmark->id = 2;
        $tempmark->order = 2;
        $tempmark->folder = "horse_festival";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Village de Toma-Ilé";
        $tempmark->posx = "89%";
        $tempmark->posy = "32%";
        $tempmark->id = 3;
        $tempmark->order = 3;
        $tempmark->folder = "toma_ile";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Village de Konkuykoro";
        $tempmark->posx = "36%";
        $tempmark->posy = "55%";
        $tempmark->id = 4;
        $tempmark->order = 4;
        $tempmark->folder = "konkuykoro";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Marché de Nouna";
        $tempmark->posx = "56%";
        $tempmark->posy = "63%";
        $tempmark->id = 5;
        $tempmark->order = 5;
        $tempmark->folder = "nouna_market";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Mosquée de Lanfiera";
        $tempmark->posx = "91%";
        $tempmark->posy = "44%";
        $tempmark->id = 6;
        $tempmark->order = 6;
        $tempmark->folder = "lanfiera_moskee";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Hippopotames du Sourou";
        $tempmark->posx = "90%";
        $tempmark->posy = "63%";
        $tempmark->id = 7;
        $tempmark->order = 7;
        $tempmark->folder = "sourou_hippos";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Village peuhl de Dokui";
        $tempmark->posx = "41%";
        $tempmark->posy = "80%";
        $tempmark->id = 8;
        $tempmark->order = 8;
        $tempmark->folder = "dokui";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Crocodiles de Ballavé";
        $tempmark->posx = "37%";
        $tempmark->posy = "94%";
        $tempmark->id = 9;
        $tempmark->order = 9;
        $tempmark->folder = "ballave_crocos";
        array_push($markers, $tempmark);

        $tempmark = new \stdClass();
        $tempmark->name = "Cimetière français de Koury";
        $tempmark->posx = "86%";
        $tempmark->posy = "63%";
        $tempmark->id = 10;
        $tempmark->order = 10;
        $tempmark->folder = "koury_graves";
        array_push($markers, $tempmark);

        // On ajoute le nouveau markers à la liste des markers
        array_push($markers, $tempmark);

        return view('accueil', [
            'markers' => $markers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
