@extends('layouts.accueillayout')

@section('content')


        <section id="banner" class=" target-section">
            <div class="row">
                <div class="column">
                    <div class="s-intro__top-block">
                        
                        <div class="row row-x-center s-intro__about-wrap">
                            <div class="column s-intro__about">
                                <p class="s-intro__about-title">
                                    <h2 id="banner_h2">Sabunyuma So: la maison de Sabunyuma</h2>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- # intro 
        ================================================== -->
        <section id="intro" class="s-intro target-section">

        <div class="row services-list block-lg-one-half block-tab-whole" data-animate-block>
                            <div >
                                <p  class="text-justify">
                                    A trois kilomètres à l'ouest de la ville de Nouna (Burkina Faso), le centre d'accueil Sabunyuma So ("la maison de Sabunyuma") propose un hébergement, de type "maison d'hôte", dans un cadre original et authentique.
                                    Sabunyuma So est situé dans un village typique de l'ethnie Marka : Koussiri.
                                    Il est entièrement construit de manière traditionnelle (en "banco"), en respectant l'architecture et les techniques ancestrales du village.
                                </p>
                            </div>
        </div>
        <div class="row">
            <div class="row folio-list block-lg-one-fourth block-tab-one-half block-stack-on-550 collapse" data-animate-block>
               <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/vkoussiri.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/vkoussiri.jpg')}}" 
                                         srcset="{{url('/images/vkoussiri.jpg')}} 1x, {{url('/images/vkoussiri.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                     </div> <!--FIN DU PREMIER BLOC -->
                 </div>

                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/mosquee.png')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/mosquee.png')}}" 
                                         srcset="{{url('/images/mosquee.png')}}1x,{{url('/images/mosquee.png')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!--FIN DU BLOC 1 -->
                </div>
    
                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/village.PNG')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/village.PNG')}}" 
                                         srcset="{{url('/images/village.PNG')}} 1x,{{url('/images/village.PNG')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!--FIN DU  BLOC 2 -->
                </div>
    
                <div class="column service-item" data-animate-el>
                    <div class="service-text">
                        <div class="column">
                            <div class="folio-item" data-animate-el>
                                <div class="folio-item__thumb">
                                    <a class="folio-item__thumb-link" href="{{url('/images/champ.PNG')}}" title="The Red Wheel" data-size="1050x700">
                                        <img src="{{url('/images/champ.PNG')}}" 
                                             srcset="{{url('/images/champ.PNG')}} 1x, {{url('/images/champ.PNG')}} 2x" alt="">
                                    </a>
                                </div>
                                <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                                </div>
                                <div class="folio-item__caption">
                                    <p></p>
                                </div>
                            </div>
                        </div> <!--FIN DU  BLOC 3 --> 
                    </div>
                </div>
    
            </div>
        </div>

        </section> <!-- FIN DE LA PARTIE INTRO DE  ACCUEIL-->


        <!-- domaine
        ================================================== -->
        <section id="domaine" class="s-services target-section">

            <div class="row section-header">
                <div class="column lg-12">
                    <h2 class="text-pretitle">La maison de Sabunyuma !</h2>
                    <h1 class="text-display-title">
                        Sabunyuma So a été <br> 
                        construit en respectant<br>
                        les traditions  Marka
                    </h1>
                </div>
            </div> <!-- fin section-header -->
    
            <div class="row services-list block-lg-one-half block-tab-whole" data-animate-block>
                <div>
                    <p class="text-justify">Les bâtiments sont en "banco" (briques de terre crue), recouverts d'un enduit à base de beurre de karité. Le centre d'accueil est composé de deux "cours" : la première est la cour d'accueil permettant de recevoir et déjeuner sous le grand "appatame" et la seconde est réservée aux logements.
                        Sabunyuma So est située à l'entrée de Koussiri, à 15 minutes en vélo de Nouna ! 
                        Le centre d'accueil dispose de sanitaires "modernes" à l'extérieur des chambres (3 WC et 3 douches) grâce à un chateau d'eau alimenté par un puits. L'éclairage est assuré grâce à des panneaux solaires.
                        Sabunyuma So, du nom des propriétaires français, Sophie et Philippe Choyer, est géré sur place et emploie plusieurs personnes du village.
                        Le centre d'accueil Sabunyuma So contribue au développement du village de Koussiri à travers l'association Dembé Sou.</p> <br> 
                        <br>
                    </div>       
                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/koussiri/sabunyuma/maison6.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/koussiri/sabunyuma/maison6.jpg')}}" 
                                         srcset="{{url('/images/koussiri/sabunyuma/maison6.jpg')}} 1x, {{url('/images/koussiri/sabunyuma/maison6.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!-- end column -->
                </div>
    
                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('images/koussiri/sabunyuma/maison5.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/koussiri/sabunyuma/maison5.jpg')}}" 
                                         srcset="{{url('/images/koussiri/sabunyuma/maison5.jpg')}} 1x, {{url('/images/koussiri/sabunyuma/maison5.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!-- end column -->
                </div>

                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/koussiri/sabunyuma/maison4.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('/images/koussiri/sabunyuma/maison4.jpg')}}" 
                                         srcset="{{url('/images/koussiri/sabunyuma/maison4.jpg 1x')}}, {{url('/images/koussiri/sabunyuma/maison4.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!-- end column -->
                </div>
    
                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/koussiri/sabunyuma/maison2.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{url('images/koussiri/sabunyuma/maison2.jpg')}}" 
                                         srcset="{{url('/images/koussiri/sabunyuma/maison2.jpg')}} 1x,{{url('/koussiri/sabunyuma/maison2.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!-- fin column -->
                </div>

                <div class="column service-item" data-animate-el>
                    <div class="column">
                        <div class="folio-item" data-animate-el>
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{url('/images/koussiri/sabunyuma/maison1.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{('/images/koussiri/sabunyuma/maison1.jpg')}}" 
                                         srcset="{{url('images/koussiri/sabunyuma/maison1.jpg')}}, koussiri/sabunyuma/maison1.jpg 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                <div class="folio-item__cat"></div>
                                <h4 class="folio-item__title"></h4>
                            </div>
                            <div class="folio-item__caption">
                                <p></p>
                            </div>
                        </div>
                    </div> <!-- end column -->
                </div>
    
    
            </div> 
        </section> <!--  FIN DOMAINE -->


        <!-- ## works
        ================================================== -->
        <section id="carte" class="s-works target-section">

            <div class="row section-header section-header--dark">
                <div class="column lg-12">
                    <h3 class="text-pretitle">Carte interactive</h3>
                    <h1 class="text-display-title">Découvrez ici Les diversités culturelles et les alentours   du village de Koussiri</h1>
                </div>
            </div> <!-- end section-header -->

            <div class="map column">
			
              <img src="{{asset('/images/map/mapv2.png')}}" alt="" />
              @foreach ($markers as $marker)
              <div  id="point" style="top: {{$marker->posy}}; left: {{$marker->posx}}; background-image: url({{asset('/images/map/pin.png')}});" class="box" data-toggle="modal" data-target="#myModal{{$marker->id}}" >
                    <div class="pin-text" >
                    <h3 >{{$marker->name}}</h3>
                    </div>
                  </div>
              @endforeach
            </div>


            	<!-- Modal -->
                @foreach ($markers as $marker)
	        <div class="modal left fade" id="myModal{{$marker->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		        <div class="modal-dialog" role="document">
			        <div class="modal-content">
				        <div class="modal-body">
					
              <section id="work" class="main style3 primary">
				        <div class="content">
					        <header>
						        <h2>{{$marker->name}}</h2>
						        <p></p>
					        </header>
					        <!-- Gallery  -->
						        <div class="gallery">
                                    @foreach(File::glob(public_path('images/'.$marker->folder).'/*') as $path)
                                            <div class="folio-item">
                                            <div class="folio-item__thumb">
                                                <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="Adama" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                                    @endforeach              
						        </div>
				        </div>
			        </section>
				        </div>

			        </div><!-- modal-content -->
		        </div><!-- modal-dialog -->
	        </div><!-- modal -->
            @endforeach

            </div> 
        </section> <!-- fin carte ineractive -->


        <!-- ## artisan
        ================================================== -->
<section id="artisan" class="s-contact target-section">

            <div class="row section-header section-header--dark">
                <div class="column lg-12">
                    <h3 class="text-pretitle">Nos artisans</h3>
                    <h1 class="text-display-title">
                        Decouvrez nos Artisans et l'art africain.
                    </h1>
                </div>
            </div>

            <div class="row about-stats stats block-lg-one-fourth block-md-one-half block-mob-whole" data-animate-block>         
                <div class="gallery-wrap">
                    <div class="item item-1" id="point" data-toggle="modal" data-target="#ModalArtisan1"></div>
                    <div class="item item-2" id="point" data-toggle="modal" data-target="#ModalArtisan2"></div>
                    <div class="item item-3" id="point" data-toggle="modal" data-target="#ModalArtisan3"></div>
                    <div class="item item-4" id="point" data-toggle="modal" data-target="#ModalArtisan4"></div>
                </div>

                 <!-- SOUS ELEMENT DE L'ACOODION EST CACHER DANS LA PAGE S'AFFICHE -->
     <div class="artisans">
             <div class="modal left fade" id="ModalArtisan1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		        <div class="modal-dialog" role="document">
			        <div class="modal-content">
				        <div class="modal-body">
					
              <section id="work" class="main style3 primary">
				        <div class="content">
					        <header>
						        <h2>Adama Sawadogo, Bronzier</h2>
						        <p>La réalisation des sculptures en bronze se fait avec le procédé dit de "la cire perdue".
                                par réaliser l'objet avec de la cire.
                                Chaque sculpture est unique, du fait même de la technique de fabrication.</p>
                                <p>
                                Une fois l'objet en cire terminé, il est entièrement recouvert d'une épaisse couche de terre  afin de former un moule autour de la cire. 
En chauffant ce moule, la cire va fondre et s'échapper par une "fenêtre" prévue à cet effet dans le moule.</p>
                                <p>Le bronze est fait à partir d'objet de récupération (robinet, canalisation), il est fondu dans un creuset.</p>
                                <p>Il ne reste plus qu'à réaliser les finitions afin de donner à la sculpture son aspects définitif et brillant (ou noircit en utilisant de l'acide).</p>
					        </header>
					        <!-- Gallery  -->
						        <div class="gallery">
                                    @foreach(File::glob(public_path('images/bronzier').'/*') as $path)
                                            <div class="folio-item">
                                            <div class="folio-item__thumb">
                                                <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="Adama" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                                    @endforeach    
						        </div>
				        </div>
			        </section>
				        </div>

			        </div><!-- modal-content -->
		        </div><!-- modal-dialog -->
	        </div><!-- modal -->
                         <div class="modal left fade" id="ModalArtisan2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		        <div class="modal-dialog" role="document">
			        <div class="modal-content">
				        <div class="modal-body">
					
              <section id="work" class="main style3 primary">
				        <div class="content">
					        <header>
						        <h2>Groupe de Griots ALASSON</h2>
						        <p>ALASSON signifie "étoile de Nouna". Les griots fabriquent eux même tous leurs instruments de musique, et composent leurs chansons.</p>
					        </header>
					        <!-- Gallery  -->
						        <div class="gallery">
                                    @foreach(File::glob(public_path('images/griots').'/*') as $path)
                                            <div class="folio-item">
                                            <div class="folio-item__thumb">
                                                <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="Adama" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                                    @endforeach  
						        </div>
				        </div>
			        </section>
				        </div>

			        </div><!-- modal-content -->
		        </div><!-- modal-dialog -->
	        </div><!-- modal -->
                         <div class="modal left fade" id="ModalArtisan3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		        <div class="modal-dialog" role="document">
			        <div class="modal-content">
				        <div class="modal-body">
					
              <section id="work" class="main style3 primary">
				        <div class="content">
					        <header>
						        <h2>Eugène KIENOU, fabricant de meubles en bois</h2>
						        <p></p>
					        </header>
					        <!-- Gallery  -->
						        <div class="gallery">
                                    @foreach(File::glob(public_path('images/fabricantM').'/*') as $path)
                                            <div class="folio-item">
                                            <div class="folio-item__thumb">
                                                <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="Adama" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                                    @endforeach  
						        </div>
				        </div>
			        </section>
				        </div>

			        </div><!-- modal-content -->
		        </div><!-- modal-dialog -->
	        </div><!-- modal -->
                         <div class="modal left fade" id="ModalArtisan4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		        <div class="modal-dialog" role="document">
			        <div class="modal-content">
				        <div class="modal-body">
					
              <section id="work" class="main style3 primary">
				        <div class="content">
					        <header>
						        <h2>Lassina DIARA, Tisserand</h2>
						        <p></p>
					        </header>
					        <!-- Gallery  -->
						        <div class="gallery">
                                    @foreach(File::glob(public_path('images/tisserant').'/*') as $path)
                                            <div class="folio-item">
                                            <div class="folio-item__thumb">
                                                <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="Adama" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                                    @endforeach  
						        </div>
				        </div>
			        </section>
				        </div>

			        </div><!-- modal-content -->
		        </div><!-- modal-dialog -->
	        </div><!-- modal -->
     </div>
  </div>    
</section>
   </div> <!-- -->
   

   <!-- photoswipe background
    ================================================== -->
    <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                    "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                    "Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                "Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>

        </div>

    </div>

    @endsection
