<!DOCTYPE html>
<html lang="en" class="no-js" >
<head>

    <!--- PAGE ACCURIL
    ================================================== -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sabunyuma So</title>

    <script>
        document.documentElement.classList.remove('no-js');
        document.documentElement.classList.add('js');
    </script>

    <!-- CSS
    ================================================== -->
    <link rel='stylesheet' href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/map.css')}}">
    <link rel="stylesheet" href="{{asset('/css/popup.css')}}">


    <!-- favicons
    ================================================== -->
    <link rel="icon" type="#" href="{{url('/images/icone.png')}}">
  
    

</head>

<body id="">


    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
        </div>
    </div>


    <!-- page wrap
    ================================================== -->
    <div class="s-pagewrap">



        <!-- # site header 
        ================================================== -->
     
            <div>
            <header class="s-header">

<div class="s-header__block">
    <div class="s-header__logo">
       <label class="logo">Sabunyuma So</label>
    </div>

    <a class="s-header__menu-toggle" href="#0"><span>Menu</span></a>
</div> <!-- end s-header__block -->

<div class="row s-header__nav-wrap">
    <nav class="s-header__nav">
        <ul>
            <li class="current"><a href="#intro" class="smoothscroll" >Accueil</a></li>
            <li><a href="#domaine" class="smoothscroll">Le Domaine</a></li>
            <li><a href="#carte" class="smoothscroll">Carte interactive</a></li>
            <li><a href="#artisan" class="smoothscroll">Nos Artisans</a></li>
            <li><a href="{{url('/koussiri')}}" >koussiri</a></li>
            <li><a href="{{url('/galerie')}}" >Galerie</a></li>
        </ul>
    </nav>

    <ul class="s-header__social">
        <li>
            <a href="https://www.facebook.com/campement.sabunyumaso">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="fill:rgba(0, 0, 0, 1);transform:;-ms-filter:"><path d="M20,3H4C3.447,3,3,3.448,3,4v16c0,0.552,0.447,1,1,1h8.615v-6.96h-2.338v-2.725h2.338v-2c0-2.325,1.42-3.592,3.5-3.592 c0.699-0.002,1.399,0.034,2.095,0.107v2.42h-1.435c-1.128,0-1.348,0.538-1.348,1.325v1.735h2.697l-0.35,2.725h-2.348V21H20 c0.553,0,1-0.448,1-1V4C21,3.448,20.553,3,20,3z"></path></svg>
                <span class="screen-reader-text">Facebook</span>
            </a>
        </li>
     
    </ul>
</div> <!-- end s-header__nav-wrap -->
</header> <!-- end s-header -->
            <main >
            @yield('content')
            </main>

            <!-- Java Script 
    ================================================== -->
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <script src="{{asset('/js/plugins.js')}}"></script>
    <script src="{{asset('/js/main.js')}}"></script>
    <script src="{{asset('/js/script.js')}}"></script>

    <script src="{{asset('/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('/js/breakpoints.min.js')}}"></script>
	<script src="{{asset('/js/map.js')}}"></script>
</body>
</html>
