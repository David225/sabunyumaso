<!DOCTYPE html>
<html lang="en" class="no-js" >
<head>

    <!--- PAGE ACCURIL
    ================================================== -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sabunyuma So</title>

    <script>
        document.documentElement.classList.remove('no-js');
        document.documentElement.classList.add('js');
    </script>

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/map.css')}}">
    <link rel="stylesheet" href="{{asset('/css/popup.css')}}">

    <!-- favicons
    ================================================== -->
    <link rel="icon" type="#" href="{{url('/images/icone.png')}}">
  
    

</head>

<body id="domaine">


    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
        </div>
    </div>


    <!-- page wrap
    ================================================== -->
    <div class="s-pagewrap">



        <!-- # site header 
        ================================================== -->
     
            <div>
            @include('layouts.partial.header')
            <main >
            @yield('content')
            </main>
            <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

<div class="pswp__bg"></div>
<div class="pswp__scroll-wrap">

    <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
    </div>

    <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
            <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
            "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
            "Zoom in/out"></button>
            <div class="pswp__preloader">
                <div class="pswp__preloader__icn">
                    <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
        </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
        "Next (arrow right)"></button>
        <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
        </div>
    </div>

</div>

</div> <!-- end photoSwipe background -->


            </div> <!-- Java Script
    ================================================== -->
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <script src="{{asset('/js/plugins.js')}}"></script>
    <script src="{{asset('/js/main.js')}}"></script>
    <script src="{{asset('/js/script.js')}}"></script>

    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
	<script src="{{asset('/js/breakpoints.min.js')}}"></script>
	<script src="{{asset('/js/map.js')}}"></script> 
</body>
</html>
