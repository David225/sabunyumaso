@extends('layouts.layout')

@section('content')





    <div class="row" id="styles">
            
                <div class="column lg-12">
                    <h3 class="text-pretitle">le village de koussiri</h3>
                    <h1 class="text-display-title" >
                  Bienvenue  à Koussiri<br> 
                </h1>
                </div>
           
                <div class="column lg-6 tab-12" >
                    <h4 class="titre"> Koussiri</h4>
                        <a href="#"> 
                        <div class="column service-item" >
                            <div class="column">
                                <div class="folio-item">
                                    <div class="folio-item__thumb">
                                        <a class="folio-item__thumb-link" href="{{asset('images/vkoussiri.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                            <img  width="200" height="170" class="u-pull-left" alt="sample-image" src="{{asset('images/vkoussiri.jpg')}}" 
                                                srcset="{{asset('images/vkoussiri.jpg')}} 1x,{{asset('images/vkoussiri.jpg')}} 2x" alt="">
                                        </a>
                                    </div>
                                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                </div>
                            </div> <!--FIN DU  BLOC 2 -->
                        </div></a>
                        <p class="paragraphe">  le village de Koussiri est un village
                        Marka particulièrement typique, d'environ 2000 habitants.Il est un des tout premiers villages de cette région, 
                        sa création remonterait au XV siècle.
                    </p>
                    </div>
                                    
                    
                    <div class="column lg-6 tab-10">
                        <h4 class="titre">Localisation</h4>
                            <div class="column service-item" >
                            <div class="column">
                                <div class="folio-item">
                                    <div class="folio-item__thumb">
                                        <a class="folio-item__thumb-link" href="{{asset('images/carte.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                            <img width="200" height="170" class="u-pull-left" alt="sample-image" src="{{asset('images/carte.jpg')}}"
                                                srcset="{{asset('images/carte.jpg')}} 1x,{{asset('images/carte.jpg')}} 2x" alt="">
                                        </a>
                                    </div>
                                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                                </div>
                            </div> <!--FIN DU  BLOC 2 -->
                        </div></a>
                        <p class="paragraphe"> Le village de Koussiri se trouve à trois kilomètres avant Nouna, sur la route qui mène à la frontière avec le Mali ( Djenné se trouve à 220 kilomètres,
                        Mopti à 300 kilomètres et Bandiagara - pays Dogon- à 360 kilomètres). On peut, dans le village de Koussiri
                        ,découvrir de très nombreuses activités traditionnelles : le filage et tissage du coton,
                        la fabrication de la bière de mil ("dolo"), la confection de savons à base de noix de karité. 
                        </p>     
                    </div>
        </div> <!-- end row -->
   </div> 
        <div class="column lg-12">
            <h1 class="text-pretitle" id="topp">L'histoire de koussiri</h1>
            <h1 class="text-display-title" id="botom" >
               Decouvrez l'origine du<br> 
                du nom Traoré<br>
            </h1>
        </div>
        
      <div class="row" >
            <div class="lg-8 tab-7 mob-12 column">
                <p class="text">
                <span class="quote"> Au début du XV siècle, le village de SOA demanda l’aide du chef  des NOUNOUMAS contre le village de KOMBARA.</span>
                    Le litige qui opposait les deux villages avait pour origine l'éxécution d'une femme enceinte originaire de Soa, 
                    mariée à Kombara. Suite à une querelle sur le sexe de l'enfant qu'elle portait,
                    les gens de Kombara lui ouvrir le ventre sur le fétiche du village afin de pouvoir clôre la discussions !
                    Evidemment la jeune femme ne survécut pas, 
                    ce qui irrita fort son village natal, Soa.
                    Grâce aux guerriers KARAMBIRI des Nounoumas, 
                    la bataille entre les deux villages fut gagnée par Soa, 
                    et le village de Kombara fut massacré.
                    Les Nounoumas sont originaire du nord de Boromo,
                    et à l'époque ils étaient réputés comme guerriers. 
                    Ils formaient alors de petites armées, en recrutant des jeunes dans les villages 
                    qu'ils traversaient dans tout l'ouest de l'actuel Burkina Faso, de Dédougou à Bobo Dioulasso 
                    (ethnies Nounoumas, Marka, Bwaba, Bobos, ...) et monnayaient leurs services contre de la nourritures ou de nouvelles recrues parmis les jeunes hommes des villages 
                    (qui devenaient alors des tara (aller) aoré (chercher), des "légionnaires", à l'origine du nom Traoré).
                   
                </p>
            </div> 
            <div class="lg-4 tab-5 mob-12 column">
            <div class="column service-item" >
                    <div class="column">
                        <div class="folio-item">
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_264.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{asset('images/coutume/image_264.jpg')}}" 
                                         srcset="{{asset('images/coutume/image_264.jpg')}} 1x,{{asset('images/coutume/image_264.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                        </div>
                    </div> <!--FIN DU  BLOC 2 -->
                </div>
                <div class="column service-item" >
                    <div class="column">
                        <div class="folio-item">
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_265.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{asset('images/coutume/image_266.jpg')}}" 
                                         srcset="{{asset('images/coutume/image_266.jpg')}} 1x,{{asset('images/coutume/image_266.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                        </div>
                    </div> <!--FIN DU  BLOC 2 -->
                </div>
            </div>
            
            <div class="lg-8 tab-7 mob-12 column">
                <p class="text">
                <span class="quote">  Avant la bataille entre Soa et Kombara et l’arrivée des guerriers Karambiri, 
                la région était occupée par les BOBOFINS. </span> Mais ceux-ci s’enfuirent et abandonnèrent leurs villages (KOUSSIRI, NOUNA KORO, TONBOBA)
                car les guerriers leur dérobèrent leurs réserves d’ignames.
                Deux frères Karambiri de Tomissankoro découvrir un jour le village abandonné de KOUSSIRI au milieu de la brousse 
                (à l’époque la région était entièrement couverte d’une brousse épaisse et remplie d’animaux sauvages ou les déplacements étaient dangereux).
                C’est ainsi donc que la famille Karambiri accompagnée de deux « étrangers » 
                (l’un originaire du village de Sanon et l’autre de celui de Bakobé – en Marka « derrière le fleuve ») vint s’y installer.
                Le nom Koussiri vient du trou ou les Bobofins conservaient leurs ignames. « Kou » signifie « igname » et « Siri » signifie « lieu de conservation »
                </p>
            </div> 
            <div class="lg-4 tab-5 mob-12 column">
                <div class="column service-item" >
                    <div class="column">
                        <div class="folio-item">
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_265.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{asset('images/coutume/image_265.jpg')}}" 
                                         srcset="{{asset('images/coutume/image_265.jpg')}} 1x,{{asset('images/coutume/image_265.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                        </div>
                    </div> <!--FIN DU  BLOC 2 -->
                </div>
            </div>
            <br>
            <br>
            <div class="lg-8 tab-7 mob-12 column">
                <p class="text">
                <span class="aliena">Les frères Karambiri décidèrent de se partager la chefferie du village,</span>
                en alternant entre les deux familles après chaque décès (au début l’élection se faisait même en présence des Karambiri de Soin, 
                mais ceux-ci laissèrent la liberté de choisir aux gens du village de Koussiri, à condition que le chef soit toujours issue de la famille traditionnelle des guerriers Karambiri).
                Le plus âgé des deux étrangers obtint pour lui et sa famille le rôle de « chef de terre » afin de pouvoir arbitrer en cas de conflit entre les familles des deux frères Karambiri  - Goussa.
                Par la suite la famille  des descendants des guerriers Nounoumas grossissant elle se divisa en deux quartiers : Kona tchibé (le quartier au bord du puits) et Badina tchibé (quartier des Badinis).
                La chefferie étant alternée entre ces deux quartiers.
                </p>
            </div> 
            <div class="lg-4 tab-5 mob-12 column">
                <div class="column service-item" >
                    <div class="column">
                        <div class="folio-item">
                            <div class="folio-item__thumb">
                                <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_267.jpg')}}" title="The Red Wheel" data-size="1050x700">
                                    <img src="{{asset('images/coutume/image_267.jpg')}}" 
                                         srcset="{{asset('images/coutume/image_267.jpg')}} 1x,{{asset('images/coutume/image_267.jpg')}} 2x" alt="">
                                </a>
                            </div>
                            <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                        </div>
                    </div> <!--FIN DU  BLOC 2 -->
                </div>
            </div>
        </div>
<section id="works" class="s-works target-section">

        <div class="row section-header section-header--dark">
            <div class="column lg-12">
                <h3 class="text-pretitle">coutume de koussiri</h3>
                <h1 class="text-display-title" >
                  La fête traditionnelle à Koussiri<br> 
                </h1>
            </div>
        </div> <!-- end section-header -->

        <div class="row folio-list block-lg-one-third block-tab-one-half block-stack-on-550 collapse" data-animate-block>
            <div class="column">
                <div class="folio-item">
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_085.jpg')}}" title="The Red Wheel" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_085.jpg')}}" 
                                srcset="{{asset('images/coutume/image_085.jpg')}} 1x, {{asset('images/coutume/image_085.jpg')}}2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_086.jpg')}}" title="Music Life" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_086.jpg')}}" 
                                srcset="{{asset('images/coutume/image_086.jpg')}} 1x, {{asset('images/coutume/image_086.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_087.jpg')}}" Title="OI Logo" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_087.jpg')}}" 
                                srcset="{{asset('images/coutume/image_087.jpg')}} 1x, {{asset('images/coutume/image_087.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_088.jpg')}}" title="Corrugated Sheets" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_088.jpg')}}" 
                                srcset="{{asset('images/coutume/image_088.jpg')}} 1x, {{asset('images/coutume/image_088.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                       <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                          <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                      </div>
                        <div class="folio-item__caption">
                           <p></p>
                        </div>    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_091.jpg')}}" title="Sand Dunes" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_91.jpg')}}" 
                                srcset="{{asset('images/coutume/image_091.jpg')}}   1x, {{asset('images/coutume/image_091.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                         <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                             <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                         </div>
                     <div class="folio-item__caption">
                       <p></p>
                     </div>                              
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_092.jpg')}}" title="The Lamp" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_092.jpg')}}" 
                                srcset="{{asset('images/coutume/image_092.jpg')}} 1x,{{asset('images/coutume/image_092.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                       <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                          <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                       </div>
                         <div class="folio-item__caption">
                             <p><!-- peut ajouter du text --></p>
                          </div>           
                 </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_093.jpg')}}" title="The Lamp" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_093.jpg')}}" 
                                srcset="{{asset('images/coutume/image_093.jpg')}} 1x,{{asset('images/coutume/image_093.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_094.jpg')}}" title="The Lamp" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_094.jpg')}}" 
                                srcset="{{asset('images/coutume/image_094.jpg')}} 1x, {{asset('images/coutume/image_094.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->
            <div class="column">
                <div class="folio-item" >
                    <div class="folio-item__thumb">
                        <a class="folio-item__thumb-link" href="{{asset('images/coutume/image_095.jpg')}}" title="The Lamp" data-size="1050x700">
                            <img src="{{asset('images/coutume/image_095.jpg')}}" 
                                srcset="{{asset('images/coutume/image_095.jpg')}} 1x, {{asset('images/coutume/image_095.jpg')}} 2x" alt="">
                        </a>
                    </div>
                    <div class="folio-item__info">
                                    <div class="folio-item__cat"><!-- peut ajouter du text --></div>
                                        <h4 class="folio-item__title"><!-- peut ajouter du text --></h4>
                                    </div>
                                    <div class="folio-item__caption">
                                        <p></p>
                                    </div>
                                    
                </div>
            </div> <!-- end column -->



        </div> <!-- folio-list -->
 
   </section> <!-- end s-works --> 
    <!-- 
    ================================================== -->
   

@endsection
