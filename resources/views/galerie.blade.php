@extends('layouts.layout')

@section('content')

        <!-- # intro 
        ================================================== -->
    <section id="works" class="hauteur">
         <div class="row section-header">
            <div class="column lg-12">
                <h3 class="actu">Dernières images de Koussiri</h3>
                    
            </div>
         </div>
        <div class="row folio-list block-lg-one-third block-tab-one-half block-stack-on-550 collapse" data-animate-block >
                @foreach(File::glob(public_path('images/news').'/*') as $path)
                 <div class="column">
                    <div class="folio-item">
                        <div class="folio-item__thumb">
                            <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                 </div>
                @endforeach  
        </div> <!-- folio-list -->

      





        <!-- # services
        ================================================== -->
        

        <!-- ## works
        ================================================== -->

            <div class="row section-header section-header--dark">
                <div class="column lg-12">
                    <h3 class="text-pretitle">Galerie</h3>
                </div>
            </div> <!-- end section-header -->

            <div class="row folio-list block-lg-one-third block-tab-one-half block-stack-on-550 collapse" data-animate-block>
                @foreach(File::glob(public_path('images/galerie').'/*') as $path)
                 <div class="column">
                    <div class="folio-item">
                        <div class="folio-item__thumb">
                            <a class="folio-item__thumb-link" href="{{url(strstr($path, 'images/'))}}" title="" data-size="1050x700">
                                                    <img src="{{url(strstr($path, 'images/'))}}" 
                                                        srcset="{{url(strstr($path, 'images/'))}} 1x,{{url(strstr($path, 'images/'))}} 2x" alt="">
                                                </a>
                                            </div>
                                            <div class="folio-item__info">
                                                <div class="folio-item__cat"></div>
                                                <h4 class="folio-item__title"></h4>
                                            </div>
                                            <div class="folio-item__caption">
                                                <p></p>
                                            </div>
                                        </div>
                 </div>
                @endforeach  
           
    
            </div> <!-- folio-list -->
          


    

    <!-- 
    ================================================== -->
    <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                    "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                    "Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                "Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>

        </div>

    </div> <!-- end photoSwipe background -->
    @endsection

